# VI Keybindings
bindkey -v

# Init ZSH package manager
if [ ! -d "${ZDOTDIR:-$HOME}/antidote" ]
then
    git clone --depth=1 https://github.com/mattmc3/antidote.git ${ZDOTDIR:-$HOME}/antidote
fi

source ${ZDOTDIR:-$HOME}/antidote/antidote.zsh
antidote load

# Init Prompt
autoload -Uz promptinit
promptinit
prompt pure

# Aliases
alias cfg='git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias ls='eza --git'
alias g='git'

# Variables
export BAT_THEME="base16"
export EDITOR="nvim"
export ZVM_INSTALL="$HOME/.zvm/self"

# HISTORY Variables
export HISTSIZE=10000000
export SAVEHIST=$HISTSIZE
export HISTFILE="$HOME/.history"

# PATH Variable
PATH="$PATH:$HOME/.local/bin"
PATH="$PATH:$HOME/.zvm/bin"
PATH="$PATH:$HOME/go/bin"
PATH="$PATH:/opt/homebrew/bin"
export PATH

# Install Nerd Fonts
if [ ! -d $HOME/.cache/nerd-fonts ]; then
    mkdir -p $HOME/.cache/;
    git clone --depth=1 https://github.com/ryanoasis/nerd-fonts $HOME/.cache/nerd-fonts

    pushd $HOME/.cache/nerd-fonts
    echo "Installing Fira Code Nerd Font"
    sudo ./install.sh 'Fira Code'
    popd
fi

# Run Direnv hook
if command -v direnv > /dev/null; then
    eval "$(direnv hook zsh)"
fi

if command -v mise > /dev/null; then 
    eval "$(mise activate zsh)"
fi

